namespace QuantConnect {

    public class MFIHelper
    {
	public decimal NegativeMoneyFlow { get; protected set; }
    	public decimal PositiveMoneyFlow { get; protected set; }
    	public decimal PreviousTypicalPrice { get; protected set; } = 0;
    	public uint Period { get; protected set; }
    	public uint Minimum { get; protected set; }
    	public uint Maximum { get; protected set; }
    	
    	public decimal MFI = 0;
    	
    	public MFIHelper(uint period = 14, uint min  = 40, uint max = 80)
    	{
    		Period = period;
    		Minimum = min;
    		Maximum = max;
    	}
    	
    	public void AddTypicalPrice(decimal high, decimal low, decimal close, decimal volume)
    	{
    		var price = (high + low + close) / 3;
    		
    		var rawMoneyFlow = price * volume;
    		
    		if(PreviousTypicalPrice > price)
    		{
    			NegativeMoneyFlow += rawMoneyFlow;
    		}
    		else
    		{
    			PositiveMoneyFlow += rawMoneyFlow;
    		}
    		
    		PreviousTypicalPrice = price;
    		
    		var moneyFlowRatio = (Period - PositiveMoneyFlow) / (Period - NegativeMoneyFlow);
    		
    		MFI = 100 - (100 / (1 + moneyFlowRatio));
    	}
    	
    	public bool IsLowerThreshold()
    	{
    		return MFI <= Minimum;
    	}
    	
    	public bool IsUpperThreshold()
    	{
    		return MFI >= Maximum;
    	}
    }
}
