namespace QuantConnect.Algorithm.CSharp
{
    public class MFIAlgorithm : QCAlgorithm
    {
    	private IDictionary<string, MFIHelper> marketSymbols = new Dictionary<string, MFIHelper>();
    	private IDictionary<string, Tuple<decimal, OrderTicket>> stopLossOrders = new Dictionary<string, Tuple<decimal, OrderTicket>>();
    	
    	protected readonly decimal PerTradeEquity = 0.01m; 	// 1%
	protected readonly decimal StopLossPercent = 0.1m;      //10% Stop Loss
	
	protected readonly decimal TrailingStopPercent = 0.05m; //Trailing Stop after 10%
        protected readonly decimal TrailingLeadPercent = 0.1m;  //at 5%
        
        private Chart stockPlot = null;
        
        public override void Initialize()
        {
		stockPlot = new Chart("MFI");
		
		SetStartDate(2015, 01, 01);  
            	SetEndDate(2018, 02, 10);    
            	SetCash(1000000);

		AddSecurityToMarket(
			type: SecurityType.Equity, 
			name: "SPY", 
			resolution:Resolution.Daily,
			helper: new MFIHelper()
		);	
		
		AddSecurityToMarket(
			type: SecurityType.Equity, 
			name: "MSFT", 
			resolution: Resolution.Daily,
			helper: new MFIHelper(
				period: 14,
				min: 29,
				max: 45
			)
		);	
        }
        
        private void AddSecurityToMarket(SecurityType type, string name, Resolution resolution, MFIHelper helper = null)
        {
		var assetPrice = new Series(name, SeriesType.Line, 0);
		
		AddSecurity(type, name, resolution);

		stockPlot.AddSeries(assetPrice);
		
		if(helper == null)
		{
			marketSymbols.Add(name, new MFIHelper());
		}
		else
		{
			marketSymbols.Add(name, helper);
		}
        }

        public override void OnData(Slice data)
        {	
            
        }
        
        public void OnData(TradeBars data) 
        {
          	foreach(var key in data.Keys)
        	{
        		var d = data[key];
        		var tuple = stopLossOrders.ContainsKey(key) ? stopLossOrders[key]  : null;
        		var mfiHelper = marketSymbols[key];
        		
        		mfiHelper.AddTypicalPrice(d.High, d.Low, d.Close, d.Volume);
        		
        		var price = d.Price;
        		
        		if(tuple == null && mfiHelper.IsLowerThreshold())
        		{
        			var equity = Portfolio.TotalPortfolioValue;
        			int quantity = Decimal.ToInt32(equity * PerTradeEquity / price);
        			
					// Buy Stock
					MarketOrder(key,  quantity, false, "BUY Threshold : " + price);
					
					var stopPrice =  price - (price * StopLossPercent);
	                var trailingLead = price + (price * TrailingLeadPercent);
                
					var _stopLoss = StopMarketOrder(key, -quantity, stopPrice, "STOP LOSS : " + stopPrice);
					
					stopLossOrders.Add(key, new Tuple<decimal, OrderTicket>(trailingLead, _stopLoss));
        		} 
        		else if(tuple != null && tuple.Item2.Status == OrderStatus.Submitted && tuple.Item1 < d.Price)
        		{
        			var trailingStopPrice = price - (price * TrailingStopPercent);
        			
        			tuple.Item2.Update(new UpdateOrderFields() { 
        				StopPrice = trailingStopPrice,
        				Tag = "Trailing Stop Loss : " + trailingStopPrice
        			});
        			
        			stopLossOrders[key] = new Tuple<decimal, OrderTicket>(price + (price * TrailingLeadPercent), tuple.Item2);
        		}
        		else if (tuple != null && tuple.Item2.Status == OrderStatus.Submitted && mfiHelper.IsUpperThreshold())
        		{
        			Liquidate(key, "Liquidate "+key+" MFI is " + mfiHelper.MFI);
        			stopLossOrders.Remove(key);
        		}
        		
	    		Plot("MFI", key, mfiHelper.MFI);
		}
	}
	
	public override void OnOrderEvent(OrderEvent orderEvent)
	{
		var order = Transactions.GetOrderById(orderEvent.OrderId);
    		Debug(string.Format("{0} >> ORDER >> " + order, Time));
	}	
    }
}
